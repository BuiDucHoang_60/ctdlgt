import java.util.*;

//Bai28 chuong 3
public class GCD {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
        int x, y;
        System.out.print("x = ");
        x = in.nextInt();
        System.out.print("y = ");
        y = in.nextInt();
        System.out.print("UCLN cua x va y la:" + UCLN(x,y));
    }
        public static int UCLN(int x,int y) {
        	if (y == 0) return x;
        	return UCLN(y , x % y);
	    }
}

