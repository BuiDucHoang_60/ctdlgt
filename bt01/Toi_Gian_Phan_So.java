import java.util.Scanner;

class Toi_Gian_Phan_So{
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.print("Nhap tu so : ");
		long a = in.nextLong();
		System.out.print("Nhap mau so : ");
		long b = in.nextLong();

		if (b == 0) {
			System.out.println("Mau so khong the bang 0 ! Nhap lai !");			
		}
		else if( b == 1){
			System.out.printf("%d/%d = %d", a, b, a);
		}
		else{
			long c = a / UCLN( a, b);
			long d = b / UCLN( a, b);
			System.out.printf("%d/%d = %d/%d", a, b, c, d);
		} 
	}

	public static long UCLN(long a, long b){

		while(a != b){
			if(a > b) a -= b;
			else b -= a;
		}
		return a;
	}
}