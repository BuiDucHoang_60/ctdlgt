//Bai 2 chuong 3

public class Quadratic {
	public static void main(String[] args) {
		double a= Double.parseDouble(args[0]);
		double b= Double.parseDouble(args[1]);
		double c= Double.parseDouble(args[2]);
		
		if (a == 0){
			if (b == 0){
				if (c == 0) {
					System.out.println("phuong trinh nghiem dung voi moi x");
				}
				else{
					System.out.println("phuong trinh da cho vo nghiem");
				}
			}
			else {
				System.out.println("Phuong trinh co nghiem x = "+ (-c/b));
			}
		} 
		else {
			double d=b*b-4*a*c;
			
			if (d < 0) System.out.println("Phuong trinh vo nghiem!");
			else if (d == 0) System.out.println("Phuong trinh co nghiem kep x = " + (-b)/(2*a));
			else {
				double sqrt = Math.sqrt(d);
				
				System.out.println("Phuong trinh co hai nghiem x1 = " + (-b+sqrt)/(2*a) + ", x2 = " + (-b-sqrt)/(2*a));
			}
		}
	}
}