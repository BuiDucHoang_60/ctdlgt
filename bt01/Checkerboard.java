import java.util.*;

//Bai 27 chuong 3
public class  Checkerboard { 
	public static void main(String[] args) { 
		int N = Integer.parseInt(args[0]); 
		InDauSao(N);
	}
	public static void InDauSao(int N){
		for(int i = 0; i < N; i++){
			if(i%2 == 1 )	
				System.out.print(" ");
			for(int j = 0; j < N; j++){
				System.out.print("* ");
			}
			System.out.println();
		}
	}
}