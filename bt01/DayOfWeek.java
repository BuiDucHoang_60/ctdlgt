import java.util.*;

//Bai 29 chuong 2

public class DayOfWeek {
	public static void main(String[] args) {
		int d, m, y;
		Scanner s = new Scanner(System.in);	
		System.out.print("Enter day/month/year: ");
		d = s.nextInt();
		m = s.nextInt();
		y = s.nextInt();

		System.out.println("Today is : " + CheckDay(d,m,y));
	}
	public static String CheckDay(int d, int m, int y){
		int y0, m0, x, d0;
		y0 = y-(14-m)/12;
		x = y0+ y0/4- y0/100+ y0/400;
		m0 = m+ 12*((14-m)/12)- 2;
		d0 = (d+x+ (31*m0)/12)% 7; 

		if (d0 == 0) return "Sunday";
		else if (d0 == 1) return "Monday";
		else if (d0 == 2) return "Tuesday";
		else if (d0 == 3) return "Wenesday";
		else if (d0 == 4) return "Thursday";
		else if (d0 == 5) return "Friday";
		return "Saturday";
	}
}
