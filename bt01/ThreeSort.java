import java.util.*;

//Bai 34 chuong 2

public class ThreeSort {
	public static void main(String[] args) {
		int a, b, c, min, max, mid;
		Scanner s = new Scanner(System.in);
		a = s.nextInt();
		b = s.nextInt();
		c = s.nextInt();
		
		min = Math.min(Math.min(a,b),c);
		max = Math.max(Math.max(a,b),c);
		mid = a + b + c - min - max;
		System.out.print("Ket qua la: " + min + " " + mid + " " + max);
	}
}