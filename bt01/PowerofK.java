import java.util.*;

public class PowerofK{
	public static long power(long k, long i){
		return (long)Math.pow(k , i);
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		long k = sc.nextLong();

		for(int i = 0; i < Long.MAX_VALUE ; i++){
			if(power(k , i) >= Long.MAX_VALUE){
				break;
			}
			else System.out.printf("\n%d ^ %d = %d",k , i, power(k, i));
		}
	}
}