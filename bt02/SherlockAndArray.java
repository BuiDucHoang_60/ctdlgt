
//bai 3
/**
 * @author BuiDucHoang
 * @version 1.0
 * @since 2016-09-24
*/

import java.io.*;
import java.util.*;

public class SherlockAndArray {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
    int t = in.nextInt();
    for(int t_i = 0; t_i  < t; t_i++)
    a: {
        int n = in.nextInt();
        int[] A = new int[n];
        for(int i = 0; i  < n; i++)
        {
            A[i] = in.nextInt();
        }
        int[] leftToRight = new int[n];
        int[] rightToLeft = new int[n];

        for(int i=1; i<n; i++)
        {
            leftToRight[i] = A[i-1] + leftToRight[i-1];
            rightToLeft[n-1-i] = A[n-i] + rightToLeft[n-i];
        }
        for(int i=0; i<n; i++)
        {
            int left = leftToRight[i];
            int right = rightToLeft[i];
            if(left == right)
            {
                System.out.println("YES");
                break a;
            }
        }
        System.out.println("NO");
    }
    }
}