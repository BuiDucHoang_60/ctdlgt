//bài 4
/**
 * @author BuiDucHoang
 * @version 1.0
 * @since 2016-09-24
*/

import java.util.*;

public class Pairs {
    public static void main(String[] args) {
        int  N, K;
        Scanner sc = new Scanner(System.in);
        N = sc.nextInt();
        K = sc.nextInt();

        int[] s = new int[N];
        for (int i = 0; i < N; i++ ){
            s[i] = sc.nextInt();
        }
        Arrays.sort(s);
        int i = 0, j = 1, count = 0;
            while(j < N) {
            int difference = s[j] - s[i];
            if (difference == K) {
                count++;
                i++;
                j++;
            } 
            else if (difference > K) {
                i++;
            } 
            else j++;
            }
        System.out.println(count);
    }
}