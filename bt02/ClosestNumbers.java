//bài 5
/**
 * @author BuiDucHoang
 * @version 1.0
 * @since 2016-09-24
*/

import java.util.*;

public class ClosestNumbers {
    public static void main(String[] args) {
        int i;
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt();
        int[] a = new int[N];
        for (i = 0; i < N; i++) {
            a[i] = sc.nextInt();
        }
        
        Arrays.sort(a);
        int min = a[1] - a[0];
        for (i = 1; i < N-1; i++) {
            if (min > a[i+1] - a[i])
                min = a[i+1] - a[i];
        }
        for (i = 0; i < N-1; i++) {
            if (a[i+1] - a[i] == min)
                System.out.print(a[i] + " " + a[i+1] + " ");
        }
    }
}