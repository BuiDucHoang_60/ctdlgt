
//bài 6
/**
 * @author BuiDucHoang
 * @version 1.0
 * @since 2016-09-24
*/import java.io.*;
import java.util.*;

public class NewYearChaos {
    
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int t = in.nextInt();
        for( int i = 0; i < t; i++ ) {
            int n = in.nextInt();
            int a[] = new int[n];
            for( int j = 0; j < n; j++ )
                a[j] = in.nextInt();
            System.out.println( bribeCount(a, n) );
        }
    }
    public static String bribeCount( int a[], int n ) {
        int i = 0, numBribes = 0;
        for( i = n-2; i >= 0; i-- ) {
            int j = i+1, t = a[i];
            int ct = 0;
            while( j < n && t > a[j] ) {
                a[j-1] = a[j]; j++; ct++;
                if( ct > 2 )
                    return "Too chaotic";
            }
            a[j-1] = t; numBribes += ct;
        }
        return Integer.toString(numBribes);
    }
}
  

